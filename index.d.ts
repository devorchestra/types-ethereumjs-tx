declare module 'ethereumjs-tx' {
     class Transaction {

        constructor(txParams:TxParameters);

        sign(privateKey:Buffer):void;

        serialize():Buffer;
    }

    interface TxParameters {
        gasLimit:string;
        to:string;
        from:string;
        value:string;
        nonce:string;
        gasPrice:string;
    }

    export = Transaction;
}